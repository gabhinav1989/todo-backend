import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import { Routes } from "./routes/routes";
import cors = require('cors');
import User from './models/userSchema';
import Email from './emailServices/sendEmail';

export class App {

    constructor() {
        // app configuration
        const app = express();
        app.use(cors());
        app.use(bodyParser.json());

        // Server settings
        const port = 3000;
        app.listen(port, () => console.log(`Example app listening on port ${port}!`));

        // routes
        const routes = new Routes();
        app.use('/', routes.myRouter);

        // mongodb connection
        const uri = 'mongodb+srv://mflix-admin:m001-mongodb-basics@mflix-jriy1.mongodb.net/mflix?retryWrites=true';
        mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true }).then(r => console.log('connected?'));

    }
}
new App();
