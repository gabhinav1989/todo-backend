import { Router } from 'express';
import User from '../models/UserSchema';
import { connect } from 'http2';
import { Period } from '../models/Period';
import TodoItem from '../models/todoItemSchema';
import Email from '../emailServices/sendEmail';

export class Routes {
    private _myRouter = Router();

    public get myRouter() {
        return this._myRouter;
    }
    public set myRouter(value) {
        this._myRouter = value;
    }

    constructor() {
        this.myRouter.get('/hi', (req, resp) => {
            resp.send('Hello world!');
        });

        this.myRouter.route('/users').get((req, res) => {
            User.find().then(data => res.json(data));
        });

        this.myRouter.route('/add').post((req, res) => {
            const name = req.body.name;
            const email = req.body.email;
            const password = req.body.password;

            const newUser = new User({ name, email, password });

            newUser.save()
                .then(() => res.json('User added!'))
                .catch(err => res.status(400).json('Error: ' + err));
        });

        this.myRouter.route('/items/add').post((req, res) => {
            const status = req.body.status;
            const date = req.body.date;
            const order = req.body.order;
            const period = req.body.period;
            const taskTitle = req.body.taskTitle;
            const instructions = req.body.instructions;
            const assignee = req.body.assignee;
            const timeEstimate = req.body.timeEstimate;
            const timeTaken = req.body.timeTaken;
            const completedBy = req.body.completedBy;
            const comments = req.body.comments;
            const links = req.body.links;

            const newItem = new TodoItem({ status, date, order, period, taskTitle, instructions, assignee, timeEstimate, timeTaken, completedBy, comments, links });
            newItem.save()
                .then(() => res.json('Items added!'))
                .catch(err => res.status(400).json('Error: ' + err));
        });


        this._myRouter.route('/items/getAll').get((req, res) => {
            TodoItem.find().then((data) => res.json(data));
        });

        // find users that match a particular criteria:
        // create a filter first and use that filter in your router.
        const test = "Abhinav";
        this.myRouter.route('/filteredUsers').get((req, res) => {
            User.find({ "name": test }).then(data => {
                res.json(data);
            });
        });

        this.myRouter.route('/sendEmail').post((req, res) => {
            const toEmail = req.body.email;
            const sub = req.body.sub;
            const text = req.body.text;
            const email = new Email(toEmail, sub, text);
            res.status(200).send("email sent");
        })
    }
}
