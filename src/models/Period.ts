export class Period {
    typeMonthEnd: boolean;
    typeQuarterEnd: boolean;
    typeMock: boolean;
    typeYearEnd: boolean;
}
