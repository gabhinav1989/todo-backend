import mongoose from 'mongoose';

export interface IUser extends mongoose.Document {
    name: string;
    somethingElse?: number;
};

export const UserSchema = new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String },
    password: { type: String }
});

const User = mongoose.model<IUser>('User', UserSchema);
export default User;


// import { createSchema, Type, typedModel } from 'ts-mongoose';

// const UserSchema = createSchema({
//     username: Type.string(),
//     email: Type.string()
// });

// const User = typedModel('User', UserSchema);