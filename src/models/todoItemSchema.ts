import {Period} from './Period';
import mongoose from 'mongoose';

export interface ITodoItem extends mongoose.Document {
    status: string;
    date: number;
    // /time: number; // Is this time that task was completed or time task needs to be completed?
    order: number;
    period: Period;
    taskTitle: string;
    instructions: string;
    assignee: string;
    timeEstimate: number;
    timeTaken: number;
    completedBy: string;
    comments: string;
    links: string;
}

export const TodoSchema = new mongoose.Schema({
    status: {type: String},
    date: {type: Date},
    order: {type: Number},
    period: {type: Object},
    taskTitle: {type: String},
    instruction: {type: String},
    assignee: {type: String},
    timeEstimate: {type: Number},
    timeTaken: {type: Number},
    completedBy: {type: String},
    comments: {type: String},
    links: {type: String},
});

const TodoItem = mongoose.model<ITodoItem>('TodoItem', TodoSchema);
export default TodoItem;
